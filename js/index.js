$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal', function(e){
        console.log("El modal se esta mostrando.");
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-danger');
        $('#contactoBtn').prop('disabled', true); // Desactiva el boton hasta que se cierre la ventana
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log("El modal se mostró.");
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log("El modal se está ocultando.");
        $('#contactoBtn').prop('disabled', false); // Activa el boton al cerrarse el modal.
        $('#contactoBtn').removeClass('btn-danger');
        $('#contactoBtn').addClass('btn-outline-success');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log("El modal se ocultó.");
    });
});